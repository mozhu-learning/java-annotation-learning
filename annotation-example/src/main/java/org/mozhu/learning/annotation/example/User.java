package org.mozhu.learning.annotation.example;

import org.mozhu.learning.annotation.CheckGetter;

@CheckGetter
public class User {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }
}
